# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
DemoApp::Application.config.secret_key_base = 'a5f0dc2b0cd66af12343e1b6e18aaac6103e43e66a1f718ce505c072e8aed2822768cbb5e14b77beba28d14ef01e1989b3b765647aa07a5dc70759484cf81345'
